import unittest

from src.mod import func

class TestCase(unittest.TestCase):
    def test_1(self):
        test = ''
        actual = func(test)

        self.assertFalse(actual)

    def test_2(self):
        test = 'Abc123'
        actual = func(test)

        self.assertTrue(actual)

    def test_3(self):
        test = ''
